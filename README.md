# README #

This is a LAMP server which builds on the bailey86/debian8 image and copies from the tutum/lamp image.  The main difference is that MySQL is by default contained within any generated container.  Obviously, this can be changed by the run command to mount MySQL data files in a volume when a container is created.

After building a container it can be logged into over SSH with the username 'root' and the password 'root'.  This may be useful to be able to copy in data files etc using scp/rsync/etc.
